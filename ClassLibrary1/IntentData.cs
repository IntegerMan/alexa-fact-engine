﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MattEland.Chat.Core
{
    public class IntentData
    {
        public IntentData(string intentName)
        {
            Name = intentName;
        }

        public string Name { get; set; }

        public string Query { get; set; }

        public int? Number { get; set; }
    }
}
