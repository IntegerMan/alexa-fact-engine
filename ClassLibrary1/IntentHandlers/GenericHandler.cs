﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MattEland.Chat.Core.Providers;

namespace MattEland.Chat.Core.IntentHandlers
{
    public class GenericHandler : IntentHandlerBase
    {
        private readonly RawReply _reply;
        private readonly FactProviderBase _provider;

        public GenericHandler([NotNull] FactProviderBase provider)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        public GenericHandler(string message) 
            : this(new RawReply(message)) // Ease of use - build a new reply out of the message
        {
            
        }

        private GenericHandler([NotNull] RawReply reply) // More complex pre-canned replies
        {
            _reply = reply ?? throw new ArgumentNullException(nameof(reply));
        }

        public override async Task<RawReply> GetResponseAsync(IntentData intentData)
        {
            // If there's a pre-canned reply, use it, otherwise talk to the provider.
            return Ok(_reply ?? await _provider.ProvideAsync(intentData));
        }
    }
}