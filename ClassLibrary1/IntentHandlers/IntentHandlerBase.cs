﻿using System.Threading.Tasks;

namespace MattEland.Chat.Core.IntentHandlers
{
    public abstract class IntentHandlerBase
    {
        protected RawReply Ok(string message, string imageUrl = null) => new RawReply(message, imageUrl);

        protected RawReply Ok(RawReply reply) => reply;
            
        protected RawReply Error(string message, string imageUrl = null) => Error(new RawReply(message, imageUrl));

        protected RawReply Error(RawReply reply) => Ok(reply); // TODO: Logging would be good


        public abstract Task<RawReply> GetResponseAsync(IntentData intentData);

    }
}