using System;
using System.Threading.Tasks;
using MattEland.Chat.Core.Providers;

namespace MattEland.Chat.Core.IntentHandlers
{
    public sealed class SimpleIntentHandler : IntentHandlerBase
    {
        private readonly RawReply _reply;

        public SimpleIntentHandler(string message, string imageUrl = null) 
            : this(new RawReply(message, imageUrl))
        {
            
        }

        public SimpleIntentHandler(RawReply reply)
        {
            _reply = reply;
        }

        public override async Task<RawReply> GetResponseAsync(IntentData intentData)
        {
            return Ok(_reply);
        }

    }
}