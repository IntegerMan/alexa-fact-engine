﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using MattEland.Chat.Core.IntentHandlers;
using MattEland.Chat.Core.Providers;
using MattEland.Chat.Core.Providers.GitHub;

namespace MattEland.Chat.Core
{
    public class IntentMapper
    {
        private readonly IDictionary<string, Func<IntentHandlerBase>> _mappings;

        public IntentMapper()
        {
            _mappings = new Dictionary<string, Func<IntentHandlerBase>>
            {
                ["hello"] = () => new GenericHandler("Hi!"),
                ["hellothere"] = () => new GenericHandler("GENERAL KENOBI!"),

                ["chucknorris"] = () => new GenericHandler(new ChuckNorrisFactProvider()),
                ["numberinfo"] = () => new GenericHandler(new NumberInfoProvider()),
                ["githubrepo"] = () => new GenericHandler(new GitHubRepositorySearchProvider()),
                ["githubrepodetails"] = () => new GenericHandler(new GitHubRepositoryDetailsProvider()),

                ["dadjoke"] = () => new GenericHandler(new DadJokeProvider()),
                ["dadtier"] = () => new SimpleIntentHandler(
                    "Dad Tier is a stream where two brothers, Isaac and Jordan, play video games. Learn more at Twitch.TV/DadTier",
                    "https://static-cdn.jtvnw.net/jtv_user_pictures/db0f16a2-3d35-4277-ae85-ad2f38bfea24-profile_image-300x300.png"),

                ["creator"] = () => new SimpleIntentHandler("I was created as a joke by Matt Eland.",
                    "https://pbs.twimg.com/profile_images/961075864740954112/Ru3qHcng_400x400.jpg")
            };
        }

        [NotNull]
        public IntentHandlerBase FindIntentHandler([NotNull] string intentName) =>
            _mappings.TryGetValue(intentName.ToLowerInvariant(), out var handlerCreator)
                ? handlerCreator()
                : new SimpleIntentHandler($"No handler was implemented for intent {intentName}.");
    }
}