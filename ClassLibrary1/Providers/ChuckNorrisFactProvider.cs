﻿using System.Web;

namespace MattEland.Chat.Core.Providers
{
    public class ChuckNorrisFactProvider : HttpClientFactProviderBase
    {

        protected override string FallbackText =>
            "Chuck Norris's keyboard only has 3 keys: 0, 1, and Deploy to Production.";

        protected override void PopulateReply(RawReply rawReply, dynamic serverFact)
        {
            if (serverFact.total != null)
            {
                if (serverFact.total <= 0)
                {
                    rawReply.Text = "Chuck Norris knows nothing about that. It must not exist.";
                }
                else
                {
                    rawReply.ImageUrl = serverFact.result[0].icon_url;
                    rawReply.Text = serverFact.result[0].value;
                }
            }
            else
            {
                rawReply.ImageUrl = serverFact.icon_url;
                rawReply.Text = serverFact.value;
            }
        }

        protected override string GetUrlForRequest(IntentData data)
        {
            return string.IsNullOrWhiteSpace(data.Query) 
                ? "https://api.chucknorris.io/jokes/random" 
                : $"https://api.chucknorris.io/jokes/search?query={HttpUtility.UrlEncode(data.Query)}";
        }
    }
}