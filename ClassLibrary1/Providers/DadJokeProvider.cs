﻿using System.Web;

namespace MattEland.Chat.Core.Providers
{
    public class DadJokeProvider : HttpClientFactProviderBase
    {
        protected override string FallbackText =>
            "You wanted a dad joke, but the joke's on you because an error occurred.";

        protected override void PopulateReply(RawReply rawReply, dynamic serverFact)
        {
            if (serverFact.results != null)
            {
                rawReply.Text = serverFact.results.Count <= 0 
                    ? "You know what I know is funny about that? Nothing! Try something else." 
                    : (string) serverFact.results[0].joke;
            }
            else
            {
                rawReply.Text = serverFact.joke;
            }
        }

        protected override string GetUrlForRequest(IntentData data) => 
            string.IsNullOrWhiteSpace(data.Query) 
                ? "https://icanhazdadjoke.com/" 
                : $"https://icanhazdadjoke.com/search?limit=1&term={HttpUtility.UrlEncode(data.Query)}";
    }
}