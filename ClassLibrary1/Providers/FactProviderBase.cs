﻿using System.Threading.Tasks;

namespace MattEland.Chat.Core.Providers
{
    public abstract class FactProviderBase
    {
        protected abstract string FallbackText { get; }
        public abstract Task<RawReply> ProvideAsync(IntentData intentData);
    }
}