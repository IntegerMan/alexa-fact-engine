﻿using JetBrains.Annotations;
using Octokit;

namespace MattEland.Chat.Core.Providers.GitHub
{
    public abstract class GitHubProviderBase : FactProviderBase
    {
        protected override string FallbackText => "Sorry; The Octocat got my tongue.";

        protected GitHubProviderBase()
        {
            // TODO: It'd likely be good to associate an API Key with this client
            Client = new GitHubClient(new ProductHeaderValue("TheBatComputer", "0.1"));
        }

        [NotNull]
        protected GitHubClient Client { get; }
    }
}