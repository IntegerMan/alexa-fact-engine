﻿using System.Threading.Tasks;

namespace MattEland.Chat.Core.Providers.GitHub
{
    public class GitHubRepositoryDetailsProvider : GitHubProviderBase
    {
        public override async Task<RawReply> ProvideAsync(IntentData intentData)
        {
            var r = await Client.Repository.Get(intentData.Number.Value);

            var text = string.IsNullOrWhiteSpace(r.Description) 
                ? $"{r.Name} by {r.Owner.Name} has no description, {r.StargazersCount} stargazer(s) and was created on {r.CreatedAt.Date.ToShortDateString()}." 
                : $"{r.FullName} has a description of {r.Description}. It has {r.StargazersCount} stargazer(s) and was created on {r.CreatedAt.Date.ToShortDateString()}.";

            return new RawReply(text) {
                Title = r.HtmlUrl
            };
        }
    }
}