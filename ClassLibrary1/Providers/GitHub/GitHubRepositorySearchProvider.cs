﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Octokit;

namespace MattEland.Chat.Core.Providers.GitHub
{
    public class GitHubRepositorySearchProvider : GitHubProviderBase
    {

        public override async Task<RawReply> ProvideAsync(IntentData intentData)
        {
            var result = await SearchAsync(intentData);

            return RespondToSearchResults(result);
        }

        private static RawReply RespondToSearchResults(SearchRepositoryResult result)
        {
            var numItems = result.TotalCount;

            if (numItems <= 0)
            {
                return new RawReply($"There are no repositories matching your search.");
            }

            // TODO: If we got here, then there's a repo. It might be nice to prompt if the user wants more information on that repo

            string text;
            var firstRepo = result.Items.First();
            switch (numItems)
            {
                case 1:
                    text = $"The only repository matching your search is {firstRepo.Name}";
                    break;

                case 2:
                    text = $"{firstRepo.Name} and 1 other repository matched your search.";
                    break;

                default:
                    text = $"{firstRepo.Name} and {numItems - 1} other repositories matched your search.";
                    break;
            }

            var rawReply = new RawReply(text)
            {
                Title = firstRepo.HtmlUrl,
                ShouldEndSession = false,
                NewIntentName = "githubrepodetails",
                NewIntentData = new Dictionary<string, string> { { "number", firstRepo.Id.ToString() } },
                // TODO: This isn't actually prompting the user to confirm what they want to do yet.
                RepropmptText = $"Would you like to learn more about {firstRepo.Name}?" // TODO: Need to be able to handle the contextual replies
            };

            return rawReply;
        }

        private async Task<SearchRepositoryResult> SearchAsync(IntentData intentData)
        {
            var searchRequest = new SearchRepositoriesRequest(intentData.Query)
            {
                PerPage = 1, // Don't need the extra DB read time on their end or a larger message size. Just 1 is fine.
                SortField = RepoSearchSort.Stars,
                // Language = Language.CSharp,
            };

            return await Client.Search.SearchRepo(searchRequest);
        }
    }
}