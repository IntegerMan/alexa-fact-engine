﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MattEland.Chat.Core.Providers
{
    public abstract class HttpClientFactProviderBase : FactProviderBase
    {
        protected abstract void PopulateReply(RawReply rawReply, dynamic serverFact);

        public override async Task<RawReply> ProvideAsync(IntentData intentData)
        {

            string url = GetUrlForRequest(intentData);

            var fact = new RawReply
            {
                Text = FallbackText
            };

            // Reach out to the API and grab a fact
            // TODO: This should be extracted out to a generic HTTP Handler class
            using (var client = new HttpClient())
            {
                // Let's make sure we get back data in JSON format
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    // Build out a message so we can add the mediaType to the content
                    var request = new HttpRequestMessage(HttpMethod.Get, url)
                    {
                        Content = new StringContent("", Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        var json = await response.Content.ReadAsStringAsync();

                        // Translate the fact to something we can understand
                        dynamic serverFact = JsonConvert.DeserializeObject(json);

                        PopulateReply(fact, serverFact);
                    }
                }
                catch (HttpRequestException rex)
                {
                    fact.Text = $"Could not reach {url}: {rex.Message}";
                }
            }

            return fact;
        }

        protected abstract string GetUrlForRequest(IntentData data);
    }
}