﻿namespace MattEland.Chat.Core.Providers
{
    public class NumberInfoProvider : HttpClientFactProviderBase
    {
        protected override string FallbackText =>
            "Numbers are fun, but an error occurred and my number is up.";
        protected override void PopulateReply(RawReply rawReply, dynamic serverFact) 
            => rawReply.Text = serverFact.text;

        protected override string GetUrlForRequest(IntentData data) 
            => data.Number.HasValue 
                ? $"http://numbersapi.com/{data.Number.Value}" 
                : "http://numbersapi.com/random";
    }
}