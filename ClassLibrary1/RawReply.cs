﻿using System.Collections.Generic;

namespace MattEland.Chat.Core
{
    public class RawReply
    {
        public RawReply(string text = null, string imageUrl = null)
        {
            Text = text;
            ImageUrl = imageUrl;
        }

        public string Title { get; set; } = "The Bat Computer";
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public bool ShouldEndSession { get; set; } = true;
        public string RepropmptText { get; set; }
        public string NewIntentName { get; set; }
        public IDictionary<string, string> NewIntentData { get; set; }
    }
}