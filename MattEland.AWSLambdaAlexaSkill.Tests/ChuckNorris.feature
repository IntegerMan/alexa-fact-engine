﻿Feature: Chuck Norris Jokes

This feature allows the user to query regarding Chuck Norris jokes

Scenario: Chuck Norris Jokes
	When The chucknorris intent is used
	Then the response should contain "Chuck Norris"
	And the image should be set

Scenario Outline: Searching Chuck Norris jokes for a term results in an expected response
	When The chucknorris intent is used with a query of "<Query>"
	Then the response should contain "<Expected>"
	And the image should not be empty

	Examples: 
	| Query									 | Expected						|
	| Keyboard							 | Keyboard						|
	| NotGoingToFindAnything | It must not exist. |
