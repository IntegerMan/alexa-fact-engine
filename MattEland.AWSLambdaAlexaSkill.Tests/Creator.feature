﻿Feature: Creator

This feature allows the user to ask who made it and get a response

Scenario: Who Created You?
	When The creator intent is used
	Then the response should contain "Matt Eland"
	And the image should be set

