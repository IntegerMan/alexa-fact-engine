﻿Feature: Random Dad Jokes

This feature allows the user to ask for a random dad joke

Scenario: Dad Joke
	When The dadjoke intent is used
	Then the response should not be empty
	And the image should be empty
	And the response should not be "You wanted a dad joke, but the joke's on you because an error occurred."

Scenario Outline: Searching Dad Jokes for a term results in an expected response
	When The dadjoke intent is used with a query of "<Query>"
	Then the response should contain "<Expected>"
	And the image should be empty

	Examples: 
	| Query									 | Expected						|
	| Dog										 | dog								|
	| NotGoingToFindAnything | Try something else |
