﻿Feature: GitHub Repository Search

This is a search provider feature allowing the user to search for repositories matching a query

Scenario: If I search for a term, I should get a response telling me how many of it there are
	When The githubrepo intent is used with a query of "Squirrel"
	Then the response should contain "reposito"
	And the image should not be set
