﻿Feature: Hello

This is a really simple feature acting as a placeholder / smoketest. 
It's the Hello There / General Kenobi meme.

Scenario: Hello -> Hi
	When The hello intent is used
	Then the response should be "Hi!"
	And the image should be empty

Scenario: Hello There -> General Kenobi
	When The hellothere intent is used
	Then the response should be "GENERAL KENOBI!"
	And the image should be empty
