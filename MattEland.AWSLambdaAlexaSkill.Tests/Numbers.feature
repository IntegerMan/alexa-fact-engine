﻿Feature: Numbers

Provide information on various numbers via an external number API

Scenario: Random Number should generate a response
	When The numberinfo intent is used
	Then the response should not be empty
	And the image should be empty

Scenario Outline: Number facts should generate a response including themselves
	When The numberinfo intent is used with a number of "<Number>"
	Then the response should contain "<Number>"
	And the image should not be set

	Examples: 
	| Number |
	| 42     |
	| -1     |
	| 0      |
	| 12345  |
