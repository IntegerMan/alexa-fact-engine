﻿using Alexa.NET.Response;
using JetBrains.Annotations;
using Shouldly;
using TechTalk.SpecFlow;

namespace MattEland.Chat.FeatureTesting
{
    [Binding]
    public partial class SpecFlowSteps
    {
        [Then(@"the response should be ""(.*)"""), UsedImplicitly]
        public void ThenTheResponseShouldSay(string response)
        {
            var text = GetTextFromResponse();

            text.ShouldBe(response);
        }

        [Then(@"the response should not be ""(.*)"""), UsedImplicitly]
        public void ThenTheResponseShouldNotSay(string response)
        {
            var text = GetTextFromResponse();

            text.ShouldNotBe(response);
        }

        [Then(@"the response should contain ""(.*)"""), UsedImplicitly]
        public void ThenTheResponseShouldContain(string response)
        {
            var text = GetTextFromResponse();

            text.ShouldContain(response);
        }

        [Then(@"the response should not contain ""(.*)"""), UsedImplicitly]
        public void ThenTheResponseShouldNotContain(string response)
        {
            var text = GetTextFromResponse();

            text.ShouldNotContain(response);
        }

        [Then(@"the response should not be empty"), UsedImplicitly]
        public void ThenTheResponseShouldBeSet()
        {
            var text = GetTextFromResponse();

            text.ShouldNotBeNull();
        }

        [Then(@"the image should be set"), Then(@"the image should not be empty"), UsedImplicitly]
        public void ThenTheImageShouldBeSet()
        {
            StandardCard card = _response.Response.Card as StandardCard;
            card.ShouldNotBeNull();

            card.Image?.SmallImageUrl.ShouldNotBeNull();
        }

        [Then(@"the image should be empty"), Then(@"the image should not be set"), UsedImplicitly]
        public void ThenTheImageShouldBeEmpty()
        {
            var card = _response.Response.Card as StandardCard;
            card.ShouldNotBeNull();

            card.Image?.SmallImageUrl.ShouldBeNull();
        }

        private string GetTextFromResponse()
        {
            var speech = (PlainTextOutputSpeech)_response.Response.OutputSpeech;

            return speech.Text ?? string.Empty;
        }

    }
}
