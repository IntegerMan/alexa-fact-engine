﻿using System.Collections.Generic;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using JetBrains.Annotations;
using MattEland.Chat.AlexaBridge;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace MattEland.Chat.FeatureTesting
{
    public partial class SpecFlowSteps
    {
        private SkillResponse _response;
        private string _json;

        [When(@"The the skill is launched"), UsedImplicitly]
        public void WhenTheTheSkillIsLaunched() =>
            ProcessRequest(new LaunchRequest {Type = "LaunchRequest"});

        [When(@"The (.*) intent is used"), UsedImplicitly]
        public void WhenTheIntentIsLaunched(string intent) =>
            ProcessRequest(new IntentRequest {Intent = new Intent {Name = intent}});

        [When(@"The (.*) intent is used with a query of ""([^""]*)"""), UsedImplicitly]
        public void WhenTheIntentIsUsedWithAQueryOf(string intent, string query)
        {
            ProcessRequest(new IntentRequest
                {
                    Intent = new Intent
                    {
                        Name = intent,
                        Slots = new Dictionary<string, Slot>
                        {
                            {"query", new Slot {Name = "query", Value = query}}
                        }
                    }
                });
        }

        [When(@"The (.*) intent is used with a number of ""([^""]*)"""), UsedImplicitly]
        public void WhenTheIntentIsUsedWithANumberQueryOf(string intent, string number)
        {
            ProcessRequest(new IntentRequest
                {
                    Intent = new Intent
                    {
                        Name = intent,
                        Slots = new Dictionary<string, Slot>
                        {
                            {"number", new Slot {Name = "number", Value = number}}
                        }
                    }
                });
        }

        [When(@"The (.*) intent is used with a source of ""([^""]*)"" and a query of ""([^""]*)"""), UsedImplicitly]
        public void WhenTheNumberinfoIntentIsUsedWithAQueryOf(string intent, string source, string query)
        {
            ProcessRequest(new IntentRequest
                {
                    Intent = new Intent
                    {
                        Name = intent,
                        Slots = new Dictionary<string, Slot>
                        {
                            {"source", new Slot {Name = "source", Value = source}},
                            {"query", new Slot {Name = "query", Value = query}}
                        }
                    }
                });
        }

        private void ProcessRequest(SkillRequest request)
        {
            var func = new Function();

            // TODO: If I add support for Google Actions, which one should I send it to? Or does it even matter - just send it to the abstraction layer?
            _response = AlexaSkillHelper.ProcessAlexaSkillRequestAsync(request).Result;

            // The serialization of this can actually fall apart quite a bit, so we want to always serialize to detect errors
            _json = JsonConvert.SerializeObject(_response);
        }

        private void ProcessRequest(Request request)
        {
            var skillRequest = new SkillRequest {Request = request};
            ProcessRequest(skillRequest);
        }
    }
}