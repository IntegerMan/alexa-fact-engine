using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Amazon.Lambda.Core;
using JetBrains.Annotations;
using MattEland.Chat.Core;
using MattEland.Shared.Collections;
using Newtonsoft.Json;

namespace MattEland.Chat.AlexaBridge
{
    public static class AlexaSkillHelper
    {
        private static readonly IntentMapper IntentMapper = new IntentMapper();

        public static async Task<SkillResponse> ProcessAlexaSkillRequestAsync(string json)
        {
            var request = JsonConvert.DeserializeObject<SkillRequest>(json);

            return await ProcessAlexaSkillRequestAsync(request);
        }

        public static async Task<SkillResponse> ProcessAlexaSkillRequestAsync(SkillRequest request)
        {
            // Log the request to CloudWatch and S3 but don't wait for it to finish
            LogSkillRequest(request);

            try
            {
                // Route to the appropriate handler
                switch (request.Request)
                {
                    case SessionEndedRequest sessionRequest:
                        return null; // Per Amazon, we're not to send back a response to these types of requests

                    case LaunchRequest launchRequest:
                        return SkillResponseBuilder.Build("This skill is still being developed.");

                    case IntentRequest intentRequest:
                        return await ProcessAlexaIntentRequestAsync(intentRequest);

                    default:
                        return SkillResponseBuilder.Build($"No intent could be found for {request.Request.Type}");
                }
            }
            catch (Exception ex)
            {
                return SkillResponseBuilder.Build($"An {ex.GetType().Name} occurred: {ex.Message}");
            }
        }

        private static async Task LogSkillRequest(SkillRequest request)
        {
            var now = DateTimeOffset.Now;
            var id = $"{now.Year}_{now.Month:##}_{now.Day:##}_{request.Session?.SessionId ?? Guid.NewGuid().ToString()}";

            LambdaLogger.Log($"Processing Alexa request {id} of type {request.Request.Type}");

#if !DEBUG
            await S3Helper.UploadToS3Asyc(id, JsonConvert.SerializeObject(request));
#endif
        }

        private static async Task<SkillResponse> ProcessAlexaIntentRequestAsync(IntentRequest intentRequest)
        {
            LambdaLogger.Log($"Processing IntentRequest {intentRequest.Intent.Name}");

            var data = ExtractIntentData(intentRequest);
            var handler = IntentMapper.FindIntentHandler(data.Name);

            var reply = await handler.GetResponseAsync(data);

            return SkillResponseBuilder.Build(reply);
        }

        private static IntentData ExtractIntentData(IntentRequest intentRequest)
        {
            var data = new IntentData(intentRequest.Intent.Name);

            var slots = intentRequest.Intent.Slots;

            slots.TryLoad("query", slot => data.Query = slot.Value);
            slots.TryLoad("number", slot => data.Number = int.Parse(slot.Value));

            return data;
        }

    }
}