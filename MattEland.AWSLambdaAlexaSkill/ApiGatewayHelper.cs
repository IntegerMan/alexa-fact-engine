using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Alexa.NET.Request;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Newtonsoft.Json;

namespace MattEland.Chat.AlexaBridge
{
    public static class ApiGatewayHelper
    {
        public static async Task<APIGatewayProxyResponse> ProcessGatewayRequestAsync(string body)
        {
            LambdaLogger.Log("Processing gateway request " + body);

            // Translate the body to a skill request
            var skillRequest = JsonConvert.DeserializeObject<SkillRequest>(body);

            // If it's bad, spit back a bad response
            if (skillRequest == null)
            {
                return CreateGatewayResponse($"Could not convert body into a SkillRequest: {body}", HttpStatusCode.BadRequest);
            }

            // Delegate for processing
            var skillResponse = await AlexaSkillHelper.ProcessAlexaSkillRequestAsync(skillRequest);

            // Wrap the response into a Gateway Response and return it
            return CreateGatewayResponse(skillResponse, HttpStatusCode.OK);
        }

        public static async Task<APIGatewayProxyResponse> ProcessGatewayRequestAsync(APIGatewayProxyRequest request) 
            => await ProcessGatewayRequestAsync(request?.Body);

        public static APIGatewayProxyResponse CreateGatewayResponse(object result, HttpStatusCode statusCode) 
            => new APIGatewayProxyResponse
            {
                StatusCode = (int) statusCode,
                Body = (result != null) ? JsonConvert.SerializeObject(result) : string.Empty,
                Headers = new Dictionary<string, string>
                {
                    { "Content-Type", "application/json" },
                    { "Access-Control-Allow-Origin", "*" }
                }
            };

    }
}