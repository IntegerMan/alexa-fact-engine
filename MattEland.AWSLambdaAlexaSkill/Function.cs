using System;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace MattEland.Chat.AlexaBridge
{
    public class Function
    {

        [NotNull]
        [UsedImplicitly] // Main entry point for AWS Lambda
        public async Task<object> FunctionHandler([CanBeNull] JObject job, [NotNull] ILambdaContext context)
        {
            try
            {
                context.Logger.LogLine("Retrieved request: " + job);

                // As a rule, if we get a null input, spit back a null output for simplicity.
                if (job == null)
                {
                    return null;
                }

                return await ProcessJObjectInputAsync(job);
            }
            catch (Exception ex)
            {
                context.Logger.LogLine($"Unhandled application error {ex.GetType().Name}: {ex.Message}");
                throw;
            }
        }

        [NotNull]
        private static async Task<object> ProcessJObjectInputAsync([NotNull] JObject job)
        {
            // If it has a body, it looks like an APIGatewayProxyRequest, which needs a different unwrap / response process
            // in order to avoid returning a 502 Bad Gateway response.
            var body = job.GetValue("body");
            if (body != null)
            {
                return await ApiGatewayHelper.ProcessGatewayRequestAsync(body.ToString());
            }

            // If it has version, it looks like an Alexa Skill, so let's try that.
            // Other things we could check for are 'session', 'context', and 'request'
            body = job.GetValue("version");
            if (body != null)
            {
                return await AlexaSkillHelper.ProcessAlexaSkillRequestAsync(job.ToString());
            }

            // Well, I don't know what it is. Hopefully this helps diagnostics.
            throw new NotSupportedException($"The provided JObject was not recognizable: {job}");
        }
    }
}
