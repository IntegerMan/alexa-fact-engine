﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda.Core;
using Amazon.Runtime.Internal.Util;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace MattEland.Chat.AlexaBridge
{
    public static class S3Helper
    {
        public static RegionEndpoint Region { get; set; } = RegionEndpoint.USEast1; // TODO: Get this from environment or context
        public static string BucketName { get; set; } = "alexa-inputs"; // TODO: Get this from environment

        public static async Task<PutObjectResponse> UploadToS3Asyc(string filename, string body)
        {
            var request = new PutObjectRequest
            {
                BucketName = BucketName,
                Key = filename,
                ContentBody = body
            };

            var client = new AmazonS3Client(Region);

            var response = await PutAsync(request, client);

            return response
;
        }

        private static async Task<PutObjectResponse> PutAsync(PutObjectRequest request, AmazonS3Client client)
        {
            try
            {
                // 1. Put object-specify only key name for the new object.
                return await client.PutObjectAsync(request);
            }
            catch (AmazonS3Exception e)
            {
                LambdaLogger.Log($"Error encountered ***. Message:'{e.Message}' when writing an object");
            }
            catch (Exception e)
            {
                LambdaLogger.Log($"Unknown error {e.GetType().Name} encountered ***. Message:'{e.Message}' when writing an object");
            }

            return null;
        }
    }
}
