﻿using System.Collections.Generic;
using Alexa.NET.Request;
using Alexa.NET.Response;
using Alexa.NET.Response.Directive;
using Amazon.Lambda.Core;
using JetBrains.Annotations;
using MattEland.Chat.Core;
using MattEland.Shared.Collections;

namespace MattEland.Chat.AlexaBridge
{
    public static class SkillResponseBuilder
    {

        [NotNull]
        public static SkillResponse Build(RawReply reply)
        {
            LambdaLogger.Log($"Building Skill Response {reply.Text}");

            return new SkillResponse
            {
                Version = "1.0",
                Response = BuildResponseBody(reply),
            };
        }

        [NotNull]
        private static ResponseBody BuildResponseBody(RawReply reply)
        {
            var body = new ResponseBody
            {
                OutputSpeech = new PlainTextOutputSpeech // TODO: SSML is also an option and could give greater control
                {
                    Text = reply.Text,
                },
                
                Card = BuildCard(reply),
                ShouldEndSession = reply.ShouldEndSession
            };

            if (!string.IsNullOrWhiteSpace(reply.NewIntentName))
            {
                body.Directives = new List<IDirective>
                {
                    new DialogDelegate {UpdatedIntent = BuildUpdatedIntent(reply)}
                };
            }

            if (!string.IsNullOrWhiteSpace(reply.RepropmptText))
            {
                body.Reprompt = new Reprompt(reply.RepropmptText);
            }

            return body;
        }

        private static Intent BuildUpdatedIntent(RawReply reply)
        {
            var intent = new Intent {Name = reply.NewIntentName};

            if (reply.NewIntentData != null)
            {
                intent.Slots = new Dictionary<string, Slot>();
                reply.NewIntentData.Each((key, value) => intent.Slots.Add(key, new Slot {Name = key, Value = value}));
            }

            return intent;
        }

        [NotNull]
        private static StandardCard BuildCard(RawReply reply)
        {
            var card = new StandardCard
            {
                Title = reply.Title,
                Content = reply.Text
            };

            // Serialization will fail if we include an image with no URL
            if (!string.IsNullOrEmpty(reply.ImageUrl))
            {
                card.Image = new CardImage
                {
                    LargeImageUrl = reply.ImageUrl,
                    SmallImageUrl = reply.ImageUrl
                };
            }

            return card;
        }

        [NotNull]
        public static SkillResponse Build(string message) => Build(new RawReply(message));
    }
}