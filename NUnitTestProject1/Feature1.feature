﻿Feature: Hello

This is a really simple feature acting as a placeholder / smoketest. It's the Hello There / General Kenobi meme.

Scenario: Hello results in a joke
	When The hello intent is used
	Then the response should say "GENERAL KENOBI!"
