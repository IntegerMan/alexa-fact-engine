﻿using System;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Amazon.Lambda.TestUtilities;
using JetBrains.Annotations;
using Shouldly;
using TechTalk.SpecFlow;

namespace MattEland.AWSLambdaAlexaSkill.Tests
{
    [Binding]
    public class HelloSteps
    {
        private SkillResponse _response;

        [When(@"The (.*) intent is used")]
        public void WhenTheIntentIsLaunched(string intent)
        {
            var func = new Function();
            var request = new SkillRequest
            {
                Request = new IntentRequest
                {
                    Intent = new Intent
                    {
                        Name = intent
                    }
                }
            };

            _response = func.FunctionHandler(request, new TestLambdaContext());
        }
        
        [Then(@"the response should say ""(.*)""")]
        public void ThenTheResponseShouldSay(string response)
        {
            var speech = (PlainTextOutputSpeech)_response.Response.OutputSpeech;

            speech.Text.ShouldBe(response);
        }
    }
}
